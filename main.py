# work for only one fastq file
# BUT could change to a loop to process all the files in database

from Bio.Seq import Seq
from Bio.Alphabet import IUPAC, ProteinAlphabet

if __name__ == "__main__":
    # original file
    file = open('Imp_stem-2_S87_R2_001.fastq')
    # sorted file (result)
    sorted_proteins = open('sorted_proteins.fastq', "w")

    Lines = file.readlines()
    total_num = 0
    protein_num = 0
    count = 1
    flag = 0
    each = ""
    for line in Lines:
        if flag == 0:
            if count == 0:
                flag = 0
            elif count == 1:
                each = ""
                total_num += 1
                title = line
                count += 1
            elif count == 2:
                indicator = Seq(line, alphabet=IUPAC.IUPACProtein())
                if isinstance(indicator.alphabet, ProteinAlphabet) == 1:
                    protein_num += 1
                    each += title
                    each += line
                    count += 1
                else:
                    flag = 1
            elif count == 3:
                each += line
                count += 1
            elif count == 4:
                each += line
                # This file will be 20+ G, "print" will save storage than "write"
                # either way will work, just move the "#" before the line
                # 1
                #sorted_proteins.write(each)
                # 2
                #print(each)
                count = 1
        elif flag == 1:
            count = 0
    file.close()
    sorted_proteins.close()
    print(total_num, "   ", protein_num)
    print(protein_num*100/total_num, "% sequence are proteins")